//
//  GlobalSandbox.h
//  EOSClient2
//
//  Created by Chang Sam on 10/20/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DefaultSandbox.h"
#import "GlobalSandboxInterface.h"
#import "LuaObjectProxyCompatible.h"
#import "LuaObjectProxy.h"
#import "LuaState.h"
#import "GlobalSandboxDelegate.h"
#import "SettingsManager.h"
#import "ESRegistry.h"

@class AppSandbox;
@class LuaHelper;

@interface GlobalSandbox : DefaultSandbox <GlobalSandboxInterface, LuaObjectProxyCompatible>{
    LuaObjectProxy *proxy;

    NSMutableDictionary *pkgApps;
    NSMutableArray *pkgStrings;
    NSFileHandle *pkgFileHandle;

    BOOL stop;
}

@property (nonatomic, strong) NSString *documentRoot;
@property (nonatomic, strong) NSString *libraryRoot;
@property (nonatomic, readonly) NSDictionary *environment;
@property (nonatomic, strong) NSString *appsRoot;
@property (nonatomic, readonly) NSString *cacheRoot;

@property (nonatomic, strong) NSString *userAgent;

//@property (nonatomic, readonly) LuaState *state;

@property (nonatomic, readonly) BOOL EOS_DEBUG_BOOL;
@property (nonatomic, readonly) BOOL EOS_DEBUGGER_BOOL;
@property (nonatomic, assign) BOOL screenAutoRotation;
@property (nonatomic, assign) UIInterfaceOrientation orientation;
@property (nonatomic, assign) UIInterfaceOrientationMask orientationMask;
@property (nonatomic, assign) BOOL statusBarHidden;
@property (nonatomic, assign) UIStatusBarStyle statusBarStyle;

@property (nonatomic, readonly) BOOL safemode;

@property (nonatomic, assign) BOOL shouldExtractDefault;

@property (nonatomic, weak) id<GlobalSandboxDelegate> delegate;

- (NSString *) getSecureCode;

- (NSString *) getSessionId;

+ (GlobalSandbox *) sandbox;

- (void) applyDefaults: (NSUserDefaults *) uds;

- (void) makeSafeMode;

- (void) preparePackage;

- (NSData *) resolveData: (NSString *) appId withPath: (NSString *) path;

@end
