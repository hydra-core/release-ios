//
//  BusyViewController.h
//  EOSFramework
//
//  Created by Sam Chang on 10/11/12.
//
//

#import <UIKit/UIKit.h>

@class RootNavViewController;

@interface RootViewController : UIViewController

@property (nonatomic, assign) RootNavViewController *navViewController;

@end
