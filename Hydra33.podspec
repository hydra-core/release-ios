Pod::Spec.new do |s|
  s.name         = "Hydra33"
  s.version      = "3.5.30"
  s.summary      = "Hydra release."
  s.description  = <<-DESC
                   Hydra release 3.x.
                   DESC
  s.homepage     = "https://bitbucket.org/hydra-core/release-ios"
  s.license      = "MIT"
  s.author       = { "samchang" => "sam.chang@me.com" }
  s.platform     = :ios, "7.0.0"
  s.source       = { :git => "https://mkgit.successinfo.com.cn/git/root/release-ios.git", :tag => "v3.5.30" }
  s.frameworks   = 'AssetsLibrary', 'MessageUI', 'AddressBookUI', 'AddressBook', 'Accelerate', 'MapKit', 'AudioToolbox', 'CoreTelephony', 'QuickLook', 'Social', 'CoreLocation'
  s.libraries = 'resolv'

  s.default_subspec = 'Release'

  s.subspec 'Release' do |ss|
    ss.resource = 'Release/builtin'
    ss.ios.vendored_frameworks = 'Release/EOS.framework'
  end

  s.subspec 'Resource' do |ss|
    ss.resource = 'Release/builtin'
  end

  s.subspec 'Debug' do |ss|
    ss.resource = 'Debug/builtin'
    ss.ios.vendored_frameworks = 'Debug/EOS.framework'
  end

  s.dependency 'DTCoreText', '~> 1.6.12'
  s.dependency 'TouchJSON', '~> 1.1'
  s.dependency 'ASIHTTPRequest', '~> 1.8.2'
  s.dependency 'CocoaAsyncSocket', '~> 7.3.5'
  s.dependency 'FMDB/common', '~> 2.3'
  s.dependency 'Reachability', '~> 3.1.1'
  s.dependency 'ZipArchive', '~> 1.4.0'
  s.dependency 'UIDevice-Hardware', '~> 0.1.3'
  s.dependency 'NSData+Base64', '~> 1.0.0'
  s.dependency 'ZBarSDK', '~> 1.3.1'
  s.dependency 'GZIP', '~> 1.0.2'
  s.dependency 'SSKeychain', '~> 1.2.2'
  s.dependency 'AnimatedGIFImageSerialization-fork', '~> 0.2.1'
  s.dependency 'iOS-WebP-fork', '~> 0.4'

  s.dependency 'lua53', '~> 0.0.8'
  s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SRCROOT)/Pods/lua53/lua-5.3.1/src $(SRCROOT)/Pods/lua53/lua53"}
end
